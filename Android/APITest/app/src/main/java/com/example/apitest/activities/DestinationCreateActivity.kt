package com.smartherd.globofly.activities

//import android.support.v7.app.AppCompatActivity
//import com.smartherd.globofly.helpers.SampleData
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import com.example.apitest.activities.addSlots
import com.example.apitest.models.DestinationRequest
import com.example.apitest.models.SlotTime
import com.example.apitest.services.DestinatioService
import com.example.apitest.services.ServiceBuilder
import com.smartherd.globofly.R
import com.smartherd.globofly.helpers.DestinationAdapter
import com.smartherd.globofly.models.Destination
import kotlinx.android.synthetic.main.activity_destiny_create.*
import kotlinx.android.synthetic.main.activity_destiny_create.toolbar
import kotlinx.android.synthetic.main.activity_destiny_list.*
import kotlinx.android.synthetic.main.add_slots.*
import kotlinx.android.synthetic.main.slot_create.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DestinationCreateActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_destiny_create)

		setSupportActionBar(toolbar)
		val context = this

		// Show the Up button in the action bar.
		supportActionBar?.setDisplayHomeAsUpEnabled(true)


		fabx.setOnClickListener {
			val intent = Intent(this@DestinationCreateActivity, addSlots::class.java)
			startActivity(intent)
		}



		val sessionService1 = ServiceBuilder.buildService(DestinatioService::class.java)
		val requestCall1 = sessionService1.getSlots()
		val slotranges = ArrayList<String>()

		requestCall1.enqueue(object: Callback<List<SlotTime>> {
			override fun onResponse(
				call: Call<List<SlotTime>>, response: Response<List<SlotTime>>
			) {
				if (response.isSuccessful)
				{
					val slots = response.body()!!
					for(slot in slots)
					{
						val str = slot.session_start.toString()
						val sessionstart = str.split("T").map { it.trim() }
						val str1 = slot.session_end.toString()
						val sessionend = str1.split("T").map { it.trim() }

						slotranges.add(slot.id.toString() + "- Date: " + sessionstart[0] +" : Start:" + sessionstart[1] + " :: End:" + sessionend[1])
					}
					//Toast.makeText(this@DestinationCreateActivity, "etrieve deslots + $slotranges", Toast.LENGTH_LONG).show()
					val adapter = ArrayAdapter(this@DestinationCreateActivity, android.R.layout.simple_spinner_item, slotranges.toArray())
					adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
					spinner2.adapter = adapter
					//spinner2.onItemSelectedListener = this@DestinationCreateActivity
					//adapter.notifyDataSetChanged()
				}
				else
				{
					Toast.makeText(this@DestinationCreateActivity, "Failed to retrieve slots", Toast.LENGTH_LONG).show()
				}
			}

			override fun onFailure(call: Call<List<SlotTime>>, t: Throwable) {
				Toast.makeText(this@DestinationCreateActivity, "Failed to retrieve slots", Toast.LENGTH_LONG).show()
			}
		})

		spinner2.onItemSelectedListener = object :
			AdapterView.OnItemSelectedListener {
			override fun onItemSelected(parent: AdapterView<*>?,
										view: View?, position: Int, id: Long) {

				Toast.makeText(this@DestinationCreateActivity,
					"Selected Slot:" + " " + "" + slotranges[position] , Toast.LENGTH_SHORT).show()
			}

			override fun onNothingSelected(parent: AdapterView<*>) {
				Toast.makeText(this@DestinationCreateActivity, "Failed to select slot", Toast.LENGTH_SHORT).show()
			}
		}
//		Toast.makeText(this, "\nSpinner 2 " + spinner2.selectedItem.toString(), Toast.LENGTH_LONG).show()








		btn_add.setOnClickListener {
			val newSession = DestinationRequest()
//			newSession.owner = et_owner.text.toString()
			newSession.subject = et_subject.text.toString()
			newSession.description = et_description.text.toString()
			val sss =  spinner2.selectedItem.toString()
			var sess_id = ""
			var slot_id = ""
			var i = 0
			while (sss[i]!='-')
			{
				sess_id += sss[i]
				i++
			}
			newSession.slotrangeid  = sess_id.toInt()



			val destinatioService1 = ServiceBuilder.buildService(DestinatioService::class.java)
			val requestCall2 = destinatioService1.getDestinationList()
			requestCall2.enqueue(object: Callback<List<Destination>> {
				override fun onResponse(
					call: Call<List<Destination>>, response: Response<List<Destination>>) {
					if (response.isSuccessful)
					{
						val destinationList = response.body()!!

						for(ids in destinationList)
						{
							Log.d("sdfrasd", "${newSession.slotrangeid} + defrgtrhd + ${ids.slotrangeid}")
							if (newSession.slotrangeid == ids.slotrangeid)
							{
								Log.d("sessionsk", "${ids.slotrangeid}")
								newSession.slotrangeid = 0
								Toast.makeText(this@DestinationCreateActivity, "Slot Occupied Select another slot", Toast.LENGTH_LONG).show()
							}
						}




						Log.d("sessionssxask", "${newSession.slotrangeid}")
						val sessionService = ServiceBuilder.buildService(DestinatioService::class.java)
						val requestCall = sessionService.addSession(newSession )
						requestCall.enqueue(object : retrofit2.Callback<Destination> {
							override fun onResponse(call: Call<Destination>, response: Response<Destination>) {
								if(response.isSuccessful)
								{
									//finish()
									var newlyCreatedSession = response.body()
									Toast.makeText(this@DestinationCreateActivity, "Session Successfully Added", Toast.LENGTH_SHORT).show()
								}
								else
									Toast.makeText(this@DestinationCreateActivity, "Failed to add session", Toast.LENGTH_SHORT).show()
							}

							override fun onFailure(call: Call<Destination>, t: Throwable) {
								Toast.makeText(this@DestinationCreateActivity, "Failed to add session", Toast.LENGTH_SHORT).show()
							}

						})



					}
					else
					{
						Toast.makeText(this@DestinationCreateActivity, "Failed to retrieve items", Toast.LENGTH_LONG).show()
					}
				}

				override fun onFailure(call: Call<List<Destination>>, t: Throwable) {
					Toast.makeText(this@DestinationCreateActivity, "Failed to retrieve items", Toast.LENGTH_LONG).show()
				}
			})


			//navigateUpTo(Intent(this, DestinationListActivity::class.java))


            //finish()
		}



	}


}
