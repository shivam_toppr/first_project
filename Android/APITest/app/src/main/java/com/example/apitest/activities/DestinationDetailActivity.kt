package com.smartherd.globofly.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
//import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.apitest.activities.addSlots
import com.example.apitest.models.SlotTime
import com.example.apitest.services.DestinatioService
import com.example.apitest.services.ServiceBuilder
import com.smartherd.globofly.R
import com.smartherd.globofly.helpers.DestinationAdapter
//import com.smartherd.globofly.helpers.SampleData
import com.smartherd.globofly.models.Destination
import kotlinx.android.synthetic.main.activity_destiny_create.*
import kotlinx.android.synthetic.main.activity_destiny_detail.*
import kotlinx.android.synthetic.main.activity_destiny_detail.et_description
import kotlinx.android.synthetic.main.activity_destiny_detail.et_subject
import kotlinx.android.synthetic.main.activity_destiny_list.*
import kotlinx.android.synthetic.main.slot_create.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class DestinationDetailActivity : AppCompatActivity() {
	public val session_id = 0
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_destiny_detail)

		setSupportActionBar(detail_toolbar)
		// Show the Up button in the action bar.
		supportActionBar?.setDisplayHomeAsUpEnabled(true)


		val sessionService1 = ServiceBuilder.buildService(DestinatioService::class.java)
		val requestCall1 = sessionService1.getSlots()
		val slotranges = ArrayList<String>()

		requestCall1.enqueue(object: Callback<List<SlotTime>> {
			override fun onResponse(
				call: Call<List<SlotTime>>, response: Response<List<SlotTime>>
			) {
				if (response.isSuccessful)
				{
					val slots = response.body()!!
					for(slot in slots)
					{
						val str = slot.session_start.toString()
						val sessionstart = str.split("T").map { it.trim() }
						val str1 = slot.session_end.toString()
						val sessionend = str1.split("T").map { it.trim() }

						slotranges.add(slot.id.toString() + "- Date: " + sessionstart[0] +" : Start:" + sessionstart[1] + " :: End:" + sessionend[1])
					}
					//Toast.makeText(this@DestinationCreateActivity, "etrieve deslots + $slotranges", Toast.LENGTH_LONG).show()
					val adapter = ArrayAdapter(this@DestinationDetailActivity, android.R.layout.simple_spinner_item, slotranges.toArray())
					adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
					spinner.adapter = adapter
					//spinner2.onItemSelectedListener = this@DestinationCreateActivity
					//adapter.notifyDataSetChanged()
				}
				else
				{
					Toast.makeText(this@DestinationDetailActivity, "Failed to retrieve slots", Toast.LENGTH_LONG).show()
				}
			}

			override fun onFailure(call: Call<List<SlotTime>>, t: Throwable) {
				Toast.makeText(this@DestinationDetailActivity, "Failed to retrieve slots", Toast.LENGTH_LONG).show()
			}
		})

		spinner.onItemSelectedListener = object :
			AdapterView.OnItemSelectedListener {
			override fun onItemSelected(parent: AdapterView<*>?,
										view: View?, position: Int, id: Long) {

				Toast.makeText(this@DestinationDetailActivity,
					"Selected Slot:" + " " + "" + slotranges[position] , Toast.LENGTH_SHORT).show()
			}

			override fun onNothingSelected(parent: AdapterView<*>) {
				Toast.makeText(this@DestinationDetailActivity, "Failed to select slot", Toast.LENGTH_SHORT).show()
			}
		}



		val bundle: Bundle? = intent.extras

		if (bundle?.containsKey(ARG_ITEM_ID)!!) {

			val id = intent.getIntExtra(ARG_ITEM_ID, 0)

			loadDetails(id)

			initUpdateButton(id)

			initDeleteButton(id)


			btn_users.setOnClickListener {
				val intent = Intent(this, SessionUsers::class.java)
				intent.putExtra("Session_ID", id)
				startActivity(intent)
			}

		}
	}
	private fun loadDetails(id: Int) {

		// To be replaced by retrofit code
		val sessionService = ServiceBuilder.buildService(DestinatioService::class.java)
		val requestCall = sessionService.getSession(id)
		requestCall.enqueue(object : retrofit2.Callback<Destination> {
			override fun onResponse(call: Call<Destination>, response: Response<Destination>) {
				if (response.isSuccessful) {
					val session = response.body()
					session?.let {
						//et_owner.setText(session.owner)
						var session_id = session.id
						et_subject.setText(session.subject)
						et_description.setText(session.description)
						val str = session.slotstart.toString()
						val sessionstart = str.split("T").map { it.trim() }
						val str1 = session.slotend.toString()
						val sessionend = str1.split("T").map { it.trim() }
						et_slotid.setText("Date: " + sessionstart[0])
						et_slotid6.setText("Start Time: " + sessionstart[1])
						et_slotid7.setText("End Time: " + sessionend[1])



						//collapsing_toolbar.title = session.subject
					}

				} else {
					Toast.makeText(
						this@DestinationDetailActivity,
						"Failed to retrieve items",
						Toast.LENGTH_LONG
					).show()
				}
			}
			override fun onFailure(call: Call<Destination>, t: Throwable) {
				Toast.makeText(this@DestinationDetailActivity, "Failed to retrieve items", Toast.LENGTH_LONG).show()
			}
		})
//		val destination = SampleData.getDestinationById(id)

	}

	private fun initUpdateButton(id: Int) {

		btn_update.setOnClickListener {

			//val owner = et_owner.text.toString()
			val subject = et_subject.text.toString()
			val description = et_description.text.toString()
			val sss =  spinner.selectedItem.toString()
			var sess_id = ""
			var slot_id = ""
			var i = 0
			while (sss[i]!='-')
			{
				sess_id += sss[i]
				i++
			}
			val slotrangeid =  sess_id.toInt()//spinner.selectedItem.toString()[0].toInt() - 48
			val sessionService = ServiceBuilder.buildService(DestinatioService::class.java)
			val requestCall = sessionService.updateSession(id, subject, description, slotrangeid)
			requestCall.enqueue(object : retrofit2.Callback<Destination> {
				override fun onResponse(call: Call<Destination>, response: Response<Destination>) {
					if(response.isSuccessful)
					{
						finish()
						Toast.makeText(this@DestinationDetailActivity, "Session Updated Successfully", Toast.LENGTH_LONG).show()
					}
					else
						Toast.makeText(this@DestinationDetailActivity, "Failed to update session", Toast.LENGTH_SHORT).show()
				}

				override fun onFailure(call: Call<Destination>, t: Throwable) {
					Toast.makeText(this@DestinationDetailActivity, "Failed to update session", Toast.LENGTH_SHORT).show()
				}

			})
		}
	}

	private fun initDeleteButton(id: Int) {

		btn_delete.setOnClickListener {

			val sessionService = ServiceBuilder.buildService(DestinatioService::class.java)
			val requestCall = sessionService.deleteSession(id)
			requestCall.enqueue(object : Callback<Unit> {
				override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
					if(response.isSuccessful)
					{
						finish()
						Toast.makeText(this@DestinationDetailActivity, "Session Deleted", Toast.LENGTH_LONG).show()
					}
					else
						Toast.makeText(this@DestinationDetailActivity, "Failed to delete session", Toast.LENGTH_SHORT).show()
				}

				override fun onFailure(call: Call<Unit>, t: Throwable) {
					Toast.makeText(this@DestinationDetailActivity, "Failed to delete session", Toast.LENGTH_SHORT).show()
				}
			})
            // To be replaced by retrofit code
//            SampleData.deleteDestination(id)
            finish() // Move back to DestinationListActivity
		}
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		val id = item.itemId
		if (id == android.R.id.home) {
			navigateUpTo(Intent(this, DestinationListActivity::class.java))
			return true
		}
		return super.onOptionsItemSelected(item)
	}

	companion object {

		const val ARG_ITEM_ID = "item_id"
	}

}
