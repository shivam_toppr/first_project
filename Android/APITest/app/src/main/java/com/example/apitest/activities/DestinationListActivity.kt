package com.smartherd.globofly.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import com.example.apitest.services.DestinatioService
import com.example.apitest.services.ServiceBuilder
//import android.support.v7.app.AppCompatActivity
import com.smartherd.globofly.R
import com.smartherd.globofly.helpers.DestinationAdapter
//import com.smartherd.globofly.helpers.SampleData
import com.smartherd.globofly.models.Destination
import kotlinx.android.synthetic.main.activity_destiny_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class DestinationListActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_destiny_list)

		setSupportActionBar(toolbar)
		toolbar.title = title

		fab.setOnClickListener {
			val intent = Intent(this@DestinationListActivity, DestinationCreateActivity::class.java)
			startActivity(intent)
		}
	}

	override fun onResume() {
		super.onResume()

		loadDestinations()
	}

	override fun onRestart() {
		super.onRestart()

		loadDestinations()
	}

	private fun loadDestinations() {

        // To be replaced by retrofit code

		val destinatioService = ServiceBuilder.buildService(DestinatioService::class.java)
		val requestCall = destinatioService.getDestinationList()
		requestCall.enqueue(object: Callback<List<Destination>> {
			override fun onResponse(
				call: Call<List<Destination>>, response: Response<List<Destination>>) {
				if (response.isSuccessful)
				{
					val destinationList = response.body()!!
					destiny_recycler_view.adapter = DestinationAdapter(destinationList)
				}
				else
				{
					Toast.makeText(this@DestinationListActivity, "Failed to retrieve items", Toast.LENGTH_LONG).show()
				}
			}

			override fun onFailure(call: Call<List<Destination>>, t: Throwable) {
				Log.v("retrofit", "call failed")
			}
		})


    }

	override fun onCreateOptionsMenu(menu: Menu?): Boolean {
		menuInflater.inflate(R.menu.menu, menu)
		val menuItem = menu!!.findItem(R.id.search)

		if(menuItem != null)
		{
			val searchView = menuItem.actionView as SearchView
			searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{

				override fun onQueryTextChange(newText: String?): Boolean {
					if(newText!!.isNotEmpty()){
						val search1 = newText.toLowerCase(Locale.getDefault())
						val destination = ArrayList<Destination>()

						val destinatioService = ServiceBuilder.buildService(DestinatioService::class.java)
						val requestCall = destinatioService.getDestinationList()
						requestCall.enqueue(object: Callback<List<Destination>> {
							override fun onResponse(
								call: Call<List<Destination>>, response: Response<List<Destination>>) {
								if (response.isSuccessful)
								{
									val destinationList = response.body()!!
									destinationList.forEach{
										if(it.subject!!.toLowerCase(Locale.getDefault()).contains(search1))
											destination.add(it)
									}
									destiny_recycler_view.adapter = DestinationAdapter(destination)
								}
								else
								{
									Toast.makeText(this@DestinationListActivity, "Failed to retrieve items", Toast.LENGTH_LONG).show()
								}
							}

							override fun onFailure(call: Call<List<Destination>>, t: Throwable) {
								Log.v("retrofit", "call failed")
							}
						})

					}
					else
					{
						val destinatioService = ServiceBuilder.buildService(DestinatioService::class.java)
						val requestCall = destinatioService.getDestinationList()
						requestCall.enqueue(object: Callback<List<Destination>> {
							override fun onResponse(
								call: Call<List<Destination>>, response: Response<List<Destination>>) {
								if (response.isSuccessful)
								{
									val destinationList = response.body()!!
									destiny_recycler_view.adapter = DestinationAdapter(destinationList)
								}
								else
								{
									Toast.makeText(this@DestinationListActivity, "Failed to retrieve items", Toast.LENGTH_LONG).show()
								}
							}

							override fun onFailure(call: Call<List<Destination>>, t: Throwable) {
								Log.v("retrofit", "call failed")
							}
						})
					}
					return true
				}

				override fun onQueryTextSubmit(query: String?): Boolean {

					return true
				}
			})
		}
		return super.onCreateOptionsMenu(menu)
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		return super.onOptionsItemSelected(item)
	}
}
