package com.example.apitest.activities
import com.smartherd.globofly.R
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.apitest.helpers.LoginDataBaseAdapter
import com.smartherd.globofly.activities.SlotList
import kotlinx.android.synthetic.main.options.*

class HomeActivity : Activity() {
    var btnSignIn: Button? = null
    var btnSignUp: Button? = null
    var loginDataBaseAdapter: LoginDataBaseAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)

// create a instance of SQLite Database
        loginDataBaseAdapter = LoginDataBaseAdapter(this)
        loginDataBaseAdapter = loginDataBaseAdapter!!.open()

// Get The Refference Of Buttons
        btnSignIn = findViewById<View>(R.id.buttonSignIN) as Button
        btnSignUp = findViewById<View>(R.id.buttonSignUP) as Button

// Set OnClick Listener on SignUp button
        btnSignUp!!.setOnClickListener { // TODO Auto-generated method stub

            /// Create Intent for SignUpActivity abd Start The Activity
            val intentSignUP =
                Intent(applicationContext, SignUPActivity::class.java)
            startActivity(intentSignUP)
        }
    }

    // Methos to handleClick Event of Sign In Button
    fun signIn(V: View?) {
        val dialog = Dialog(this@HomeActivity)
        dialog.setContentView(R.layout.options)
        dialog.setTitle("Login")


        val editTextUserName =
            dialog.findViewById<View>(R.id.editTextUserNameToLogin) as EditText
        val editTextPassword =
            dialog.findViewById<View>(R.id.editTextPasswordToLogin) as EditText
        val btnSignIn =
            dialog.findViewById<View>(R.id.buttonSignIn) as Button


        btnSignIn.setOnClickListener { // get The User name and Password
            val userName = editTextUserName.text.toString()
            val password = editTextPassword.text.toString()

            // fetch the Password form database for respective user name
            val storedPassword: String = loginDataBaseAdapter!!.getSinlgeEntry(userName)

            // check if the Stored password matches with Password entered by user
            if (password == storedPassword) {

                Toast.makeText(
                    this@HomeActivity,
                    "Congrats: Login Successfull",
                    Toast.LENGTH_LONG
                ).show()
                dialog.dismiss()
                val user = userName
                Log.d("userr", user)
                val intent = Intent(this, SlotList::class.java)
                intent.putExtra("UserName", user)
                startActivity(intent)
            } else {
                Toast.makeText(
                    this@HomeActivity,
                    "User Name or Password does not match",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        dialog.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        // Close The Database
        loginDataBaseAdapter!!.close()
    }
}