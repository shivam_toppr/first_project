package com.smartherd.globofly.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.apitest.services.DestinatioService
import com.example.apitest.services.ServiceBuilder
//import android.support.v7.app.AppCompatActivity
import com.smartherd.globofly.R
import com.smartherd.globofly.helpers.DestinationAdapter
import com.smartherd.globofly.helpers.SlotAdapter
//import com.smartherd.globofly.helpers.SampleData
import com.smartherd.globofly.models.Destination
import com.smartherd.globofly.models.Slot
import kotlinx.android.synthetic.main.activity_destiny_create.*
import kotlinx.android.synthetic.main.activity_destiny_list.*
import kotlinx.android.synthetic.main.activity_destiny_list.toolbar
import kotlinx.android.synthetic.main.session_users_list.*
import kotlinx.android.synthetic.main.slot_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SessionUsers : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.session_users_list)

		setSupportActionBar(toolbar5)
		val context = this

		// Show the Up button in the action bar.
		supportActionBar?.setDisplayHomeAsUpEnabled(true)


	}

	override fun onResume() {
		super.onResume()

		loadDestinations()
	}

	private fun loadDestinations() {

        // To be replaced by retrofit code
		val sess_id = intent.getIntExtra("Session_ID", 0)
		Toast.makeText(this@SessionUsers, "$sess_id", Toast.LENGTH_LONG).show()
		val destinatioService = ServiceBuilder.buildService(DestinatioService::class.java)
		val requestCall = destinatioService.getSessionUsers(sess_id)
		requestCall.enqueue(object: Callback<List<Slot>> {
			override fun onResponse(
				call: Call<List<Slot>>, response: Response<List<Slot>>) {
				if (response.isSuccessful)
				{
					val slotList = response.body()!!
					destiny_recycler_view5.adapter = SlotAdapter(slotList)
				}
				else
				{
					Toast.makeText(this@SessionUsers, "Failed to retrieve items", Toast.LENGTH_LONG).show()
				}
			}

			override fun onFailure(call: Call<List<Slot>>, t: Throwable) {
				Log.v("retrofit", "call failed")
			}
		})


    }
}
