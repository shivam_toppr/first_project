package com.example.apitest.activities
import com.smartherd.globofly.R
import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.apitest.helpers.LoginDataBaseAdapter

class SignUPActivity : Activity() {
    var editTextUserName: EditText? = null
    var editTextPassword: EditText? = null
    var editTextConfirmPassword: EditText? = null
    var btnCreateAccount: Button? = null
    var loginDataBaseAdapter: LoginDataBaseAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

// get Instance of Database Adapter
        loginDataBaseAdapter = LoginDataBaseAdapter(this)
        loginDataBaseAdapter = loginDataBaseAdapter!!.open()

// Get Refferences of Views
        editTextUserName = findViewById<View>(R.id.editTextUserName) as EditText
        editTextPassword = findViewById<View>(R.id.editTextPassword) as EditText
        editTextConfirmPassword =
            findViewById<View>(R.id.editTextConfirmPassword) as EditText
        btnCreateAccount =
            findViewById<View>(R.id.buttonCreateAccount) as Button
        btnCreateAccount!!.setOnClickListener(View.OnClickListener {
            // TODO Auto-generated method stub
            val userName = editTextUserName!!.text.toString()
            val password = editTextPassword!!.text.toString()
            val confirmPassword = editTextConfirmPassword!!.text.toString()

            // check if any of the fields are vaccant
            if (userName == "" || password == "" || confirmPassword == "") {
                Toast.makeText(applicationContext, "Field Vaccant", Toast.LENGTH_LONG)
                    .show()
                return@OnClickListener
            }
            // check if both password matches
            if (password != confirmPassword) {
                Toast.makeText(
                    applicationContext,
                    "Password does not match",
                    Toast.LENGTH_LONG
                ).show()
                return@OnClickListener
            } else {
                // Save the Data in Database
                loginDataBaseAdapter!!.insertEntry(userName, password)
                Toast.makeText(
                    applicationContext,
                    "Account Successfully Created ",
                    Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    override fun onDestroy() {
// TODO Auto-generated method stub
        super.onDestroy()
        loginDataBaseAdapter!!.close()
    }
}