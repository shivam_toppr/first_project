package com.smartherd.globofly.activities

//import android.support.v7.app.AppCompatActivity
//import com.smartherd.globofly.helpers.SampleData
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import com.example.apitest.models.DestinationRequest
import com.example.apitest.models.SlotRequest
import com.example.apitest.models.SlotTime
import com.example.apitest.services.DestinatioService
import com.example.apitest.services.ServiceBuilder
import com.smartherd.globofly.R
import com.smartherd.globofly.helpers.SlotAdapter
import com.smartherd.globofly.models.Destination
import com.smartherd.globofly.models.Slot
import kotlinx.android.synthetic.main.activity_destiny_create.*
import kotlinx.android.synthetic.main.activity_destiny_create.btn_add
import kotlinx.android.synthetic.main.activity_destiny_create.toolbar
import kotlinx.android.synthetic.main.slot_create.*
import kotlinx.android.synthetic.main.slot_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SlotCreate : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.slot_create)

		setSupportActionBar(toolbar3)
		val context = this

		// Show the Up button in the action bar.
		supportActionBar?.setDisplayHomeAsUpEnabled(true)







		val sessionService1 = ServiceBuilder.buildService(DestinatioService::class.java)
		val requestCall1 = sessionService1.getDestinationList()
		val sessions_list = ArrayList<String>()

		requestCall1.enqueue(object: Callback<List<Destination>> {
			override fun onResponse(
				call: Call<List<Destination>>, response: Response<List<Destination>>
			) {
				if (response.isSuccessful)
				{
					val sessions = response.body()!!
					for(session in sessions)
					{
						val str = session.slotstart.toString()
						val sessionstart = str.split("T").map { it.trim() }
						val str1 = session.slotend.toString()
						val sessionend = str1.split("T").map { it.trim() }
						sessions_list.add(session.id.toString() + "-" + session.slotrangeid.toString() + ":" + session.subject + " : " + session.description+ "\n Date: " + sessionstart[0] + "\nStart: " + sessionstart[1] + " :: End:" + sessionend[1])
					}
					//Toast.makeText(this@DestinationCreateActivity, "etrieve deslots + $slotranges", Toast.LENGTH_LONG).show()
					val adapter = ArrayAdapter(this@SlotCreate, android.R.layout.simple_spinner_item, sessions_list.toArray())
					adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
					spinner4.adapter = adapter
					//spinner2.onItemSelectedListener = this@DestinationCreateActivity
					//adapter.notifyDataSetChanged()
				}
				else
				{
					Toast.makeText(this@SlotCreate, "Failed to retrieve slots", Toast.LENGTH_LONG).show()
				}
			}

			override fun onFailure(call: Call<List<Destination>>, t: Throwable) {
				Toast.makeText(this@SlotCreate, "Failed to retrieve slots", Toast.LENGTH_LONG).show()
			}
		})

		spinner4.onItemSelectedListener = object :
			AdapterView.OnItemSelectedListener {
			override fun onItemSelected(parent: AdapterView<*>?,
										view: View?, position: Int, id: Long) {
				val sss= spinner4.selectedItem.toString()[0].toInt() - 48
				Toast.makeText(this@SlotCreate,
					"Subscribed Session:" + " " + "" + sessions_list[position] + ":::" + sss , Toast.LENGTH_SHORT).show()
			}

			override fun onNothingSelected(parent: AdapterView<*>) {
				Toast.makeText(this@SlotCreate, "Failed to select slot", Toast.LENGTH_SHORT).show()
			}
		}
//		Toast.makeText(this, "\nSpinner 2 " + spinner2.selectedItem.toString(), Toast.LENGTH_LONG).show()











		btn_subs.setOnClickListener {
			val newSlot = SlotRequest()
//			newSession.owner = et_owner.text.toString()
			val sss =  spinner4.selectedItem.toString()
			var sess_id = ""
			var slot_id = ""
			var i = 0
			while (sss[i]!='-')
			{
				sess_id += sss[i]
				i++
			}
			i++
			while (sss[i]!=':')
			{
				slot_id += sss[i]
				i++
			}
			Log.d("seslot", "$sess_id + $slot_id")



			val user = intent.getStringExtra("UserN")
			Log.d("user5", "$user")


			newSlot.owner = user
			newSlot.session  = sess_id.toInt()


			val destinatioService1 = ServiceBuilder.buildService(DestinatioService::class.java)
			val requestCall1 = destinatioService1.getSlotList()
			requestCall1.enqueue(object: Callback<List<Slot>> {
				override fun onResponse(
					call: Call<List<Slot>>, response: Response<List<Slot>>) {
					if (response.isSuccessful)
					{
						val slotList = response.body()!!
						Log.d("sots", "$slotList")
						Log.d("swss", "${newSlot.session}")
						Log.d("swsss", "${slot_id.toInt()}")

						for(ids in slotList)
						{
							if (slot_id.toInt() == ids.slotrangeid  && ids.owner==user)
							{
								Log.d("sessionsk", "${slot_id.toInt()}")
								newSlot.session = 0
								Toast.makeText(this@SlotCreate, "Slot Occupied Select another slot", Toast.LENGTH_LONG).show()
							}
						}


						val sessionService = ServiceBuilder.buildService(DestinatioService::class.java)
						val requestCall = sessionService.addSlot(newSlot)
						requestCall.enqueue(object : retrofit2.Callback<Slot> {
							override fun onResponse(call: Call<Slot>, response: Response<Slot>) {
								if(response.isSuccessful)
								{
									//finish()
									var newlyCreatedSlot = response.body()
									Toast.makeText(this@SlotCreate, "Session Successfully Subscribed", Toast.LENGTH_SHORT).show()
								}
								else
									Toast.makeText(this@SlotCreate, "Failed to subscribe session", Toast.LENGTH_SHORT).show()
							}

							override fun onFailure(call: Call<Slot>, t: Throwable) {
								Toast.makeText(this@SlotCreate, "Failed to add session", Toast.LENGTH_SHORT).show()
							}

						})

						//finish()


					}
					else
					{
						Toast.makeText(this@SlotCreate, "Failed to retrieve items", Toast.LENGTH_LONG).show()
					}
				}

				override fun onFailure(call: Call<List<Slot>>, t: Throwable) {
					Log.v("retrofit", "call failed")
				}
			})



            //finish()
		}
	}

}
