package com.smartherd.globofly.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
//import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.apitest.models.SlotTime
import com.example.apitest.services.DestinatioService
import com.example.apitest.services.ServiceBuilder
import com.smartherd.globofly.R
import com.smartherd.globofly.helpers.DestinationAdapter
//import com.smartherd.globofly.helpers.SampleData
import com.smartherd.globofly.models.Destination
import com.smartherd.globofly.models.Slot
import kotlinx.android.synthetic.main.activity_destiny_create.*
import kotlinx.android.synthetic.main.activity_destiny_detail.*
import kotlinx.android.synthetic.main.activity_destiny_detail.et_description
import kotlinx.android.synthetic.main.activity_destiny_detail.et_subject
import kotlinx.android.synthetic.main.activity_destiny_list.*
import kotlinx.android.synthetic.main.slot_detail.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SlotDetail : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.slot_detail)

		setSupportActionBar(detail_toolbar1)
		// Show the Up button in the action bar.
		supportActionBar?.setDisplayHomeAsUpEnabled(true)


		val bundle: Bundle? = intent.extras

		if (bundle?.containsKey(ARG_ITEM_ID1)!!) {

			val id = intent.getIntExtra(ARG_ITEM_ID1, 0)

			loadDetails(id)

			initDeleteButton(id)
		}
	}

	private fun loadDetails(id: Int) {

		// To be replaced by retrofit code
		val sessionService = ServiceBuilder.buildService(DestinatioService::class.java)
		val requestCall = sessionService.getSlot(id)
		requestCall.enqueue(object : retrofit2.Callback<Slot> {
			override fun onResponse(call: Call<Slot>, response: Response<Slot>) {
				if (response.isSuccessful) {
					val session = response.body()
					session?.let {
						//et_owner.setText(session.owner)
						val str = session.slotstart.toString()
						val sessionstart = str.split("T").map { it.trim() }
						val str1 = session.slotend.toString()
						val sessionend = str1.split("T").map { it.trim() }
						et_subject1.setText(session.subject)
						et_description1.setText(session.description)
						et_slotid1.setText("Date: " +sessionstart[0])
						et_slotid2.setText("Start Time: "+sessionstart[1])
						et_slotid3.setText("End Time: "+sessionend[1])


						//collapsing_toolbar.title = session.subject
					}

				} else {
					Toast.makeText(
						this@SlotDetail,
						"Failed to retrieve items",
						Toast.LENGTH_LONG
					).show()
				}
			}
			override fun onFailure(call: Call<Slot>, t: Throwable) {
				Toast.makeText(this@SlotDetail, "Failed to retrieve items", Toast.LENGTH_LONG).show()
			}
		})
//		val destination = SampleData.getDestinationById(id)

	}


	private fun initDeleteButton(id: Int) {

		btn_delete1.setOnClickListener {
			val sessionService = ServiceBuilder.buildService(DestinatioService::class.java)
			val requestCall = sessionService.deleteSlot(id)
			requestCall.enqueue(object : Callback<Unit> {
				override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
					if(response.isSuccessful)
					{
						finish()
						Toast.makeText(this@SlotDetail, "Slot Unsubscribed", Toast.LENGTH_LONG).show()
					}
					else
						Toast.makeText(this@SlotDetail, "Failed to delete slot", Toast.LENGTH_SHORT).show()
				}

				override fun onFailure(call: Call<Unit>, t: Throwable) {
					//print("sdsfg")
					Toast.makeText(this@SlotDetail, "Failed to delete slot", Toast.LENGTH_SHORT).show()
				}
			})
            // To be replaced by retrofit code
//            SampleData.deleteDestination(id)
            finish() // Move back to DestinationListActivity
		}
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		val id = item.itemId
		if (id == android.R.id.home) {
			navigateUpTo(Intent(this, SlotList::class.java))
			return true
		}
		return super.onOptionsItemSelected(item)
	}

	companion object {

		const val ARG_ITEM_ID1 = "item_id"
	}
}
