package com.smartherd.globofly.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.apitest.services.DestinatioService
import com.example.apitest.services.ServiceBuilder
//import android.support.v7.app.AppCompatActivity
import com.smartherd.globofly.R
import com.smartherd.globofly.helpers.DestinationAdapter
import com.smartherd.globofly.helpers.SlotAdapter
//import com.smartherd.globofly.helpers.SampleData
import com.smartherd.globofly.models.Destination
import com.smartherd.globofly.models.Slot
import kotlinx.android.synthetic.main.activity_destiny_list.*
import kotlinx.android.synthetic.main.slot_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SlotList : AppCompatActivity() {


	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.slot_list)

		setSupportActionBar(toolbar1)
		toolbar1.title = title

		val user =  intent.getStringExtra("UserName")
		Log.d("user7", "$user")
		fab1.setOnClickListener {
			val intent = Intent(this@SlotList, SlotCreate::class.java)
			intent.putExtra("UserN", user)
			startActivity(intent)
		}
	}

	override fun onResume() {
		super.onResume()

		loadDestinations()
	}
	override fun onRestart() {
		super.onRestart()

		loadDestinations()
	}
	private fun loadDestinations() {

        // To be replaced by retrofit code
		val user =  intent.getStringExtra("UserName")
		Log.d("user1", "$user")
		val destinatioService = ServiceBuilder.buildService(DestinatioService::class.java)
		val requestCall = destinatioService.getSlotList()
		requestCall.enqueue(object: Callback<List<Slot>> {
			override fun onResponse(
				call: Call<List<Slot>>, response: Response<List<Slot>>) {
				if (response.isSuccessful)
				{
					val slotList = response.body()!!
					val slots = ArrayList<Slot>()
					for(slot in slotList)
					{
						if(slot.owner == user )
							slots += slot

					}
					Log.v("slotList", "${slotList}")
					Log.v("slotListnew", "${slots}")
					destiny_recycler_view1.adapter = SlotAdapter(slots)
				}
				else
				{
					Toast.makeText(this@SlotList, "Failed to retrieve items", Toast.LENGTH_LONG).show()
				}
			}

			override fun onFailure(call: Call<List<Slot>>, t: Throwable) {
				Log.v("retrofit", "call failed")
			}
		})


    }
}
