package com.smartherd.globofly.activities

//import android.support.v7.app.AppCompatActivity
//import com.example.apitest.activities.login
import android.content.Intent
import android.icu.util.Calendar
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.apitest.activities.HomeActivity
import com.example.apitest.models.SlotTime
import com.example.apitest.services.DestinatioService
import com.example.apitest.services.ServiceBuilder
import com.smartherd.globofly.R
import com.smartherd.globofly.models.Destination
import kotlinx.android.synthetic.main.activity_destiny_detail.*
import kotlinx.android.synthetic.main.activity_welcome.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class WelcomeActivity : AppCompatActivity(){

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_welcome)

		message.text = "CRUD Operations on Session"

		val destinatioService = ServiceBuilder.buildService(DestinatioService::class.java)
		val requestCall = destinatioService.getDestinationList()

		requestCall.enqueue(object: Callback<List<Destination>> {
			@RequiresApi(Build.VERSION_CODES.N)
			override fun onResponse(
				call: Call<List<Destination>>, response: Response<List<Destination>>) {
				if (response.isSuccessful)
				{
					val destinationList = response.body()!!
					val c = Calendar.getInstance()
					val year = c.get(Calendar.YEAR)
					var month = c.get(Calendar.MONTH)
					val day = c.get(Calendar.DAY_OF_MONTH)
					val hour = c.get(Calendar.HOUR_OF_DAY)
					val min = c.get(Calendar.MINUTE)

					month = month + 1

					Log.d("currtime", "$year")
					Log.d("currtime1", "$month")
					Log.d("currtime2", "$day")
					Log.d("currtime3", "$hour")
					Log.d("currtime4", "$min")

					for(sessions in destinationList)
					{
						val str = sessions.slotstart.toString()
						val sessionstart = str.split("T").map { it.trim() }
						val str1 = sessions.slotend.toString()
						val sessionend = str1.split("T").map { it.trim() }
						val sd = sessionstart[0].split("-").map { it.trim() }
						val ed = sessionend[0].split("-").map { it.trim() }
						val st = sessionstart[1].split(":").map { it.trim() }
						val et = sessionend[1].split(":").map { it.trim() }
						Log.d("sdate", "$sd")
						Log.d("stime", "$st")
						Log.d("sid", "${sessions.id}")
						if(sd[1].toInt()< month)
							sessions.id?.let { deleteInvalidUsers(it) }
						if(sd[0].toInt()< year)
							sessions.id?.let { deleteInvalidUsers(it) }
						if(sd[2].toInt()< day)
							sessions.id?.let { deleteInvalidUsers(it) }
						else if (sd[2].toInt()==day)
						{
							if(st[0].toInt()<hour)
								sessions.id?.let { deleteInvalidUsers(it) }
							else if(st[0].toInt()==hour)
							{
								if(st[1].toInt()<=min)
									sessions.id?.let { deleteInvalidUsers(it) }
							}

						}

					}

				}
				else
				{
					Log.v("retrofit", "call failed")
				}
			}

			override fun onFailure(call: Call<List<Destination>>, t: Throwable) {
				Log.v("retrofit", "call failed")
			}
		})





		val sessionService1 = ServiceBuilder.buildService(DestinatioService::class.java)
		val requestCall1 = sessionService1.getSlots()
		val slotranges = ArrayList<String>()

		requestCall1.enqueue(object: Callback<List<SlotTime>> {
			@RequiresApi(Build.VERSION_CODES.N)
			override fun onResponse(
				call: Call<List<SlotTime>>, response: Response<List<SlotTime>>
			) {
				if (response.isSuccessful)
				{
					val c = Calendar.getInstance()
					val year = c.get(Calendar.YEAR)
					var month = c.get(Calendar.MONTH)
					val day = c.get(Calendar.DAY_OF_MONTH)
					val hour = c.get(Calendar.HOUR_OF_DAY)
					val min = c.get(Calendar.MINUTE)

					month = month + 1

					val slots = response.body()!!
					for(slot in slots)
					{
						val str = slot.session_start.toString()
						val sessionstart = str.split("T").map { it.trim() }
						val str1 = slot.session_end.toString()
						val sessionend = str1.split("T").map { it.trim() }
						val sd = sessionstart[0].split("-").map { it.trim() }
						val ed = sessionend[0].split("-").map { it.trim() }
						val st = sessionstart[1].split(":").map { it.trim() }
						val et = sessionend[1].split(":").map { it.trim() }

						if(sd[1].toInt()< month)
							slot.id?.let { deleteInvalidSlotRanges(it) }
						if(sd[0].toInt()< year)
							slot.id?.let { deleteInvalidSlotRanges(it) }
						if(sd[2].toInt()< day)
							slot.id?.let { deleteInvalidSlotRanges(it) }
						else if (sd[2].toInt()==day)
						{
							if(st[0].toInt()<hour)
								slot.id?.let { deleteInvalidSlotRanges(it) }
							else if(st[0].toInt()==hour)
							{
								if(st[1].toInt()<=min)
									slot.id?.let { deleteInvalidSlotRanges(it) }
							}

						}

					}
				}
				else
				{
					Log.v("retrofit", "delete failed")
				}
			}

			override fun onFailure(call: Call<List<SlotTime>>, t: Throwable) {
				Log.v("retrofit", "delete failed")
			}
		})
	}




	fun getAdmin(view: View) {

		val intent = Intent(this, DestinationListActivity::class.java)
		startActivity(intent)
		finish()
	}



	fun getUser(view: View) {

		val intent = Intent(this, HomeActivity::class.java)
		startActivity(intent)
		finish()
	}




	fun deleteInvalidUsers(id:Int)
	{
		val sessionService = ServiceBuilder.buildService(DestinatioService::class.java)
		val requestCall = sessionService.deleteSession(id)
		requestCall.enqueue(object : Callback<Unit> {
			override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
				if(response.isSuccessful)
				{
					Log.d("deleted", "Invalid sessions deleted")
				}
				else
					Log.d("notdeleted1", "Invalid sessions not deleted")
			}

			override fun onFailure(call: Call<Unit>, t: Throwable) {
				Log.d("notdeleted", "Invalid sessions not deleted")
			}
		})
	}




	fun deleteInvalidSlotRanges(id:Int)
	{
		val sessionService = ServiceBuilder.buildService(DestinatioService::class.java)
		val requestCall = sessionService.deleteSlotRange(id)
		requestCall.enqueue(object : Callback<Unit> {
			override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
				if(response.isSuccessful)
				{
					Log.d("deleted", "Invalid slot ranges deleted")
				}
				else
					Log.d("notdeleted1", "Invalid slot ranges not deleted")
			}

			override fun onFailure(call: Call<Unit>, t: Throwable) {
				Log.d("notdeleted", "Invalid slot ranges not deleted")
			}
		})
	}
}
