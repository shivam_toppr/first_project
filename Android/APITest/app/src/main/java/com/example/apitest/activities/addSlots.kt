package com.example.apitest.activities

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.icu.util.Calendar
import android.os.Build
import android.os.Bundle
import android.text.format.DateFormat.is24HourFormat
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.smartherd.globofly.R
import kotlinx.android.synthetic.main.add_slots.*
//import java.text.DateFormat
import android.text.format.DateFormat
import android.util.Log
import android.widget.Toast
import com.example.apitest.models.DestinationRequest
import com.example.apitest.models.SlotAddRequest
import com.example.apitest.models.SlotTime
import com.example.apitest.services.DestinatioService
import com.example.apitest.services.ServiceBuilder
import com.smartherd.globofly.models.Destination
import com.smartherd.globofly.models.Slot
import kotlinx.android.synthetic.main.activity_destiny_create.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import javax.xml.datatype.DatatypeConstants.MONTHS

class addSlots : AppCompatActivity(){
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_slots)
        sess_date.setOnClickListener { view ->
            ClickDatePicker(view)
        }
        start.setOnClickListener { view ->
            StartTimePicker(view)
        }
        end.setOnClickListener { view ->
            EndTimePicker(view)
        }

        btn_slot.setOnClickListener {
            val newDate = SlotAddRequest()
//			newSession.owner = et_owner.text.toString()
            val sdate = sess_date.text.toString()
            val stime = start.text.toString()
            val etime = end.text.toString()
            newDate.session_start =  sdate + "T" + stime
            newDate.session_end = sdate + "T" + etime


            val sessionService1 = ServiceBuilder.buildService(DestinatioService::class.java)
            val requestCall1 = sessionService1.getSlots()
            val slotranges = ArrayList<String>()

            requestCall1.enqueue(object: Callback<List<SlotTime>> {
                @RequiresApi(Build.VERSION_CODES.N)
                override fun onResponse(
                    call: Call<List<SlotTime>>, response: Response<List<SlotTime>>
                ) {
                    if (response.isSuccessful)
                    {
                        val stt = stime.split(":").map { it.trim() }
                        val ett = etime.split(":").map { it.trim() }
                        val starttimecurr = stt[0].toInt()*60 + stt[1].toInt()
                        val endtimecurr = ett[0].toInt()*60 + ett[1].toInt()

                        val slots = response.body()!!
                        var flag = true
                        for(slot in slots)
                        {
                            val str = slot.session_start.toString()
                            val sessionstart = str.split("T").map { it.trim() }
                            val str1 = slot.session_end.toString()
                            val sessionend = str1.split("T").map { it.trim() }
                            val st = sessionstart[1].split(":").map { it.trim() }
                            val et = sessionend[1].split(":").map { it.trim() }

                            val starttime = st[0].toInt()*60 + st[1].toInt()
                            val endtime = et[0].toInt()*60 + et[1].toInt()
                            if(sessionstart[0] == sdate)
                            {
                                if((starttimecurr in starttime..endtime) || (endtimecurr in starttime..endtime))
                                {
                                    flag = false
                                    break
                                }
                            }

                        }
                        if(flag)
                        {

                            val sessionService = ServiceBuilder.buildService(DestinatioService::class.java)
                            val requestCall = sessionService.addSlots(newDate)
                            requestCall.enqueue(object : retrofit2.Callback<Slot> {
                                override fun onResponse(call: Call<Slot>, response: Response<Slot>) {
                                    if(response.isSuccessful)
                                    {
                                        finish()
                                        var newlyCreatedSession = response.body()
                                        Toast.makeText(this@addSlots, "Slot Successfully Added", Toast.LENGTH_SHORT).show()
                                    }
                                    else
                                        Toast.makeText(this@addSlots, "Failed to add slot, Slot time can't be in Past", Toast.LENGTH_SHORT).show()
                                }

                                override fun onFailure(call: Call<Slot>, t: Throwable) {
                                    Toast.makeText(this@addSlots, "Failed to add slot", Toast.LENGTH_SHORT).show()
                                }

                            })
                        }
                        else
                            Toast.makeText(this@addSlots, "Slot Conflict occurred, Select different time", Toast.LENGTH_SHORT).show()


                    }
                    else
                    {
                        Log.v("retrofit", "Delete failed")
                    }
                }

                override fun onFailure(call: Call<List<SlotTime>>, t: Throwable) {
                    Log.v("retrofit", "delete failed")
                }
            })



            finish()
        }




    }
    @RequiresApi(Build.VERSION_CODES.N)
    fun ClickDatePicker(view: View){
        val c = Calendar.getInstance()
        var year = c.get(Calendar.YEAR)
        var month = c.get(Calendar.MONTH)
        var day = c.get(Calendar.DAY_OF_MONTH)


        DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            // Display Selected date in textbox
            var monthOfYear = monthOfYear + 1
            var monthOf = monthOfYear.toString()
            if(monthOfYear<10)
                monthOf = '0' + monthOfYear.toString()

            var dayOf = dayOfMonth.toString()
            if(dayOfMonth<10)
                dayOf = '0' + dayOf.toString()


            Log.d("yr", "${dayOf}")
            Log.d("mnth", "${monthOf}")
            Log.d("day", "${year}")

            sess_date.setText(year.toString() + "-" + monthOf + "-" + dayOf)

        }, year, month, day).show()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun StartTimePicker(view: View){
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR)
        val min = c.get(Calendar.MINUTE)

        TimePickerDialog(this, TimePickerDialog.OnTimeSetListener{ view, starthour, startmin ->

            // Display Selected date in textbox
            var shour = starthour.toString()
            if(starthour<10)
                shour = '0' + shour

            var smin = startmin.toString()
            if(startmin<10)
                smin = '0' + smin


            Log.d("hr", "${shour}")
            Log.d("mn", "${smin}")
            start.setText("" + shour + ":" + smin + ":00" )

        }, hour, min, true).show()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun EndTimePicker(view: View){
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR)
        val min = c.get(Calendar.MINUTE)


        TimePickerDialog(this, TimePickerDialog.OnTimeSetListener{ view, starthour, startmin ->

            // Display Selected date in textbox
            var shour = starthour.toString()
            if(starthour<10)
                shour = '0' + shour

            var smin = startmin.toString()
            if(startmin<10)
                smin = '0' + smin


            Log.d("hr", "${shour}")
            Log.d("mn", "${smin}")
            end.setText("" + shour + ":" + smin + ":00" )

        }, hour, min, true).show()
    }


}