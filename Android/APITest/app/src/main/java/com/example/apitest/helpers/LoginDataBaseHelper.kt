package com.example.apitest.helpers

import android.content.ContentValues
import android.content.Context
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase


class LoginDataBaseAdapter(// Context of the application using the database.
    private val context: Context
) {
    // Variable to hold the database instance
    var databaseInstance: SQLiteDatabase? = null

    // Database open/upgrade helper
    private val dbHelper: DataBaseHelper

    @Throws(SQLException::class)
    fun open(): LoginDataBaseAdapter {
        databaseInstance = dbHelper.writableDatabase
        return this
    }

    fun close() {
        databaseInstance!!.close()
    }

    fun insertEntry(userName: String?, password: String?) {
        val newValues = ContentValues()
        // Assign values for each row.
        newValues.put("USERNAME", userName)
        newValues.put("PASSWORD", password)

// Insert the row into your table
        databaseInstance!!.insert("LOGIN", null, newValues)
        ///Toast.makeText(context, "Reminder Is Successfully Saved", Toast.LENGTH_LONG).show();
    }

    fun deleteEntry(UserName: String): Int {
//String id=String.valueOf(ID);
        val where = "USERNAME=?"
        // Toast.makeText(context, "Number fo Entry Deleted Successfully : "+numberOFEntriesDeleted, Toast.LENGTH_LONG).show();
        return databaseInstance!!.delete("LOGIN", where, arrayOf(UserName))
    }

    fun getSinlgeEntry(userName: String): String {
        val cursor = databaseInstance!!.query(
            "LOGIN",
            null,
            " USERNAME=?",
            arrayOf(userName),
            null,
            null,
            null
        )
        if (cursor.count < 1) // UserName Not Exist
        {
            cursor.close()
            return "NOT EXIST"
        }
        cursor.moveToFirst()
        val password = cursor.getString(cursor.getColumnIndex("PASSWORD"))
        cursor.close()
        return password
    }

    fun updateEntry(userName: String, password: String?) {
// Define the updated row content.
        val updatedValues = ContentValues()
        // Assign values for each row.
        updatedValues.put("USERNAME", userName)
        updatedValues.put("PASSWORD", password)
        val where = "USERNAME = ?"
        databaseInstance!!.update("LOGIN", updatedValues, where, arrayOf(userName))
    }

    companion object {
        const val DATABASE_NAME = "login.db"
        const val DATABASE_VERSION = 1
        const val NAME_COLUMN = 1

        // TODO: Create public field for each column in your table.
        // SQL Statement to create a new database.
        const val DATABASE_CREATE = "create table " + "LOGIN" +
                "( " + "ID" + " integer primary key autoincrement," + "USERNAME text,PASSWORD text); "
    }

    init {
        dbHelper = DataBaseHelper(
            context,
            DATABASE_NAME,
            null,
            DATABASE_VERSION
        )
    }
}