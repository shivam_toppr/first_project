package com.smartherd.globofly.helpers

import android.content.Intent
//import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.smartherd.globofly.activities.SlotList
import com.smartherd.globofly.models.Slot
import com.smartherd.globofly.R
import com.smartherd.globofly.activities.SlotDetail


class SlotAdapter(private val slotList: List<Slot>) : RecyclerView.Adapter<SlotAdapter.ViewHolder>() {

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

		val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_slots, parent, false)
		return ViewHolder(view)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {

		holder.slot = slotList[position]
		holder.txvSlot.text = slotList[position].subject + "::" + slotList[position].description + " (" + slotList[position].owner + ")"

		holder.itemView.setOnClickListener { v ->
			val context = v.context
			val intent = Intent(context, SlotDetail::class.java)
			intent.putExtra(SlotDetail.ARG_ITEM_ID1, holder.slot!!.id)

			context.startActivity(intent)
		}
	}

	override fun getItemCount(): Int {
		return slotList.size
	}

	class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

		val txvSlot: TextView = itemView.findViewById(R.id.txv_slot)
		var slot: Slot? = null

		override fun toString(): String {
			return """${super.toString()} '${txvSlot.text}'"""
		}
	}
}
