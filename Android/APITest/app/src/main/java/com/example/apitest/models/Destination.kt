package com.smartherd.globofly.models

import java.util.*

data class Destination(
	var id: Int? = 0,
	var owner: String? = null,
	var subject: String? = null,
	var description: String? = null,
	var slotrangeid: Int?= 0,
	var slotstart: String?= null,
	var slotend: String? = null

	)
