package com.example.apitest.models
import java.util.*
class DestinationRequest {
    var owner: String? = null
    var subject: String? = null
    var description: String? = null
    var slotrangeid: Int? = 0
}