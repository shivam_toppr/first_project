package com.smartherd.globofly.models

import java.util.*

data class Slot(
	var id: Int? = 0,
	var owner: String? = null,
	var session: Int?= 0,
	var subject: String? = null,
	var description: String? = null,
	val slotrangeid: Int? = 0,
	var slotstart: String?= null,
	var slotend: String? = null

	)
