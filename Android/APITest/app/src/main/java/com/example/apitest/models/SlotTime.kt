package com.example.apitest.models

class SlotTime {
    var id: Int? = 0
    var session_start: String? = null
    var session_end: String? = null
}