package com.smartherd.globofly.models

import java.util.*

data class User(
	//var id: Int? = 0,
	var username: String? = null,
	var email: String? = "abcd@gmail.com",
	var password: String? = null,
	var is_staff: Boolean? = false

	)
