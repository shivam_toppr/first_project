package com.example.apitest.services

import com.example.apitest.models.DestinationRequest
import com.example.apitest.models.SlotAddRequest
import com.example.apitest.models.SlotRequest
import com.example.apitest.models.SlotTime
import com.smartherd.globofly.models.Destination
import com.smartherd.globofly.models.Slot
import com.smartherd.globofly.models.User
import retrofit2.Call
import retrofit2.http.*

interface DestinatioService {
    @GET("sessions")
    fun getDestinationList() : Call<List<Destination>>
    //fun getDestinationList(@Query("slotrangeid") slotrangeid: Int?) : Call<List<Destination>>

    @GET("addSlots")
    fun getSlots() : Call<List<SlotTime>>

    @POST("addSlots/")
    fun addSlots(@Body newDate: SlotAddRequest) : Call<Slot>

    @DELETE("addSlots/{id}/")
    fun deleteSlotRange(
        @Path("id") id: Int) : Call<Unit>


    @GET("sessions/{id}")
    fun getSession(@Path("id") id:Int) : Call<Destination>

    @POST("sessions/")
    fun addSession(@Body newSession: DestinationRequest) : Call<Destination>

    @FormUrlEncoded
    @PUT("sessions/{id}/")
    fun updateSession(
        @Path("id") id:Int,
        @Field("subject") subject: String,
        @Field("description") description: String,
        @Field("slotrangeid") slotrangeid: Int
    ) : Call<Destination>


    @DELETE("sessions/{id}/")
    fun deleteSession(
        @Path("id") id: Int) : Call<Unit>


    @GET("students_slots")
    fun getSlotList() : Call<List<Slot>>

    @GET("students_slots/{id}")
    fun getSlot(@Path("id") id:Int) : Call<Slot>

    @POST("students_slots/")
    fun addSlot(@Body newSlot: SlotRequest) : Call<Slot>

    @DELETE("students_slots/{id}/")
    fun deleteSlot(
        @Path("id") id: Int) : Call<Unit>

    @GET("sessionDetails/{id}")
    fun getSessionUsers(@Path("id") id:Int) : Call<List<Slot>>

    @POST("api-auth/login/")
    fun getUser(@Body userdetails: User) : Call<User>
    //fun addSession(@Body newSession: DestinationRequest) : Call<Destination>

}