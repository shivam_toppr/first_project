from django.shortcuts import render
from .serializers import registerSerializers
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User

@api_view(['GET', 'POST'])
def register_view(request):
    if request.method == 'GET':
        register = User.objects.all()
        serializer = registerSerializers(register,many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = registerSerializers(data=request.data)
        if serializer.is_valid():
            account=serializer.save()
            return Response(serializer.data,status=201)
        return Response(serializer.errors,status=400)    

@api_view(['GET', 'PUT','DELETE'])
def register_detail(request,pk):
    try:
        register = User.objects.get(pk=pk)
    except register.DoesNotExist:
        return Response(status=status.HTTP_204_NO_CONTENT)
    if request.method == 'GET':
        serializer = registerSerializers(register)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = registerSerializers(register,data=request.data)
        if serializer.is_valid():
            account=serializer.save()
            return Response(serializer.data,status=201)
        return Response(serializer.errors,status=400)
    elif request.method == "DELETE":
        register.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
# Create your views here.
