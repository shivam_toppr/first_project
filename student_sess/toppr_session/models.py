from django.db import models
from django.contrib.auth.models import User
import datetime
from django.conf import settings
from django.core.exceptions import ValidationError

def no_past(value):
    today = datetime.date.today()
    if value <= today:
        raise ValidationError('Session Date cannot be Today or in the Past.')
# Create your models here.


class SlotRange(models.Model):
    SLOT_CHOICES = (
        ('slot1 - 9-10 Hrs', '9AM-10AM'),
        ('slot2 - 10-11 Hrs', '10AM-11AM'),
        ('slot3 - 11-12 Hrs', '11AM-12PM'),
        ('slot4 - 12-13 Hrs', '12PM-1PM'),
        ('slot5 - 13 -14 Hrs', '1PM-2PM'),
        ('slot6 - 14-15 Hrs', '2PM-3PM'),
        ('slot7 - 15-16 Hrs', '3PM-4PM'),
        ('slot8 - 16-17 Hrs', '4PM-5PM'),
        ('slot9 - 17-18 Hrs', '5PM-6PM'),
        ('slot10 - 18-19 Hrs', '6PM-7PM'),
    )
    sr = models.CharField(max_length=5, choices=SLOT_CHOICES)

    def __str__(self):
        return self.sr


class Session(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='sessioncreator', on_delete=models.CASCADE)
    subject = models.CharField(max_length=50)
    description = models.CharField(max_length=500)
    session_date = models.DateField(help_text="Enter the date of session", validators=[no_past])
    session_slot = models.ManyToManyField(SlotRange)

    def __str__(self):
        return self.subject + ":" + self.description


class Slot(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='slotuser', on_delete=models.CASCADE)
    session_name = models.ForeignKey(Session, on_delete=models.CASCADE)

    class Meta:
        unique_together= ("owner", "session_name")
    def __str__(self):
        return str(self.session_name.session_slot.all())


# class SessionDetails(models.Model):
#     sessionid = models.ForeignKey(Session, on_delete=models.CASCADE)
#     slotid = models.ForeignKey(Slot, on_delete=models.CASCADE)

#     def __str__(self):
#         return self.session_id.subject + ":" + self.session_id.description + " " + self.slot_id.owner.username