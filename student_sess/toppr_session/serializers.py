from rest_framework import serializers
from .models import Session, SlotRange, Slot
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model

class UserSerializer(serializers.ModelSerializer):
    details = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password', 'details')
        extra_kwargs = {
            'password': {
                'write_only': True
            }
        }


class SessionSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    class Meta:
        model = Session
        fields = '__all__'


class SlotRangeSerializer(serializers.ModelSerializer):
    class Meta:
        model = SlotRange
        fields = '__all__'


class SlotSerializer(serializers.ModelSerializer):
    #user = serializers.StringRelatedField()
    #session_name =  serializers.SlugRelatedField(read_only=True, slug_field='subject')
    #details = serializers.ReadOnlyField(source='session_name.description')
    owner = serializers.ReadOnlyField(source='owner.username')
    class Meta:
        model = Slot
        fields = '__all__'


# class SessionDetailsSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = SessionDetail
#         fields = '__all__'