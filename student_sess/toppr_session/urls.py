from django.contrib import admin
from django.urls import path, include
from .views import SessionView, SlotRangeView, SessionDetailView, SlotView#, UserViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register("sessions", SessionView)
#router.register("session_detail", SessionDetailView)
router.register("students_slots", SlotView)
router.register("slots", SlotRangeView)
#router.register("students", UserViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path('sessionDetails/<int:pk>', SessionDetailView.as_view()),
    #path('slotSelect/', SlotView.as_view()),
    #path('register/', create_auth),
    #path('showsession/', ShowSession.as_view()),
]