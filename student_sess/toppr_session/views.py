from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from rest_framework import viewsets, permissions, generics
import json
import logging
from .models import Session, Slot, SlotRange
from .serializers import SessionSerializer, SlotSerializer, SlotRangeSerializer, UserSerializer
from .permissions import IsOwnerOrReadOnly, IsStaffOrTargetUser
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response



class SessionView(viewsets.ModelViewSet):
    queryset = Session.objects.all().order_by('-subject')
    serializer_class = SessionSerializer
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        permissions.IsAdminUser
    )

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)



class SessionDetailView(APIView):
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)
    def get_object(self, pk):
        try:
            return Slot.objects.filter(session_name__id=pk).select_related('session_name__owner')
        except Slot.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        slot = self.get_object(pk)
        serializer = SlotSerializer(slot, many=True)
        return Response(serializer.data)

    # queryset = Slot.objects.all()
    # serializer_class = SlotSerializer
    # permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)
    
    # def get_queryset(self):
    #     # sess_id = self.request.query_params.get('session_name__id')
    #     # print(sess_id)
    #     # queryset = Slot.objects.filter(session_name__id=1).select_related('session_name__owner')
    #     queryset = Slot.objects.select_related('session_name__owner')
    #     # if self.kwargs["pk"]:
    #     #     queryset = queryset.objects.filter(session_name__id=self.kwargs["pk"])

    #     return queryset



class SlotView(viewsets.ModelViewSet):
    queryset = Slot.objects.all().order_by('-id')
    serializer_class = SlotSerializer
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly
    )


    def get_queryset(self):
        user = self.request.user#query_params.get('session_name__session_slot')
        queryset = self.queryset.filter(owner=user)#.select_related('session_name__session_slot')
        #slot_id = self.request.query_params.get('session_name__session_slot')
        #print(slot_id)
        #queryset = self.queryset.filter(session_name__session_slot=slot_id)
        for e in queryset:
            print(e)
        return queryset
    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)




    # def perform_create(self, serializer):
    #     serializer.save(owner=self.request.user)




class SlotRangeView(viewsets.ModelViewSet):
    queryset = SlotRange.objects.all()
    serializer_class = SlotRangeSerializer
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)



'''
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        # allow non-authenticated user to create via POST
        return (permissions.AllowAny() if self.request.method == 'POST' else IsStaffOrTargetUser()),

    def perform_create(self, serializer):
        password = make_password(self.request.data['password'])
        serializer.save(password=password)

    def perform_update(self, serializer):
        password = make_password(self.request.data['password'])
        serializer.save(password=password)



class ShowSession(generics.ListAPIView):
    serializer_class = SlotSerializer
    def get_queryset(self):
        queryset = Slot.objects.all()
        student =  self.request.query_params.get('student', None)
        administrator =  self.request.query_params.get('administrator', None)
        print(student)
        print(administrator)
        if student is not None and administrator is not None:
            try:
                queryset=queryset.filter( Q(user_id=student) | Q(user_id=administrator))
            except:
                queryset=None
        return queryset
'''

'''
@api_view(['POST'])
def create_auth(request):
    serialized = UserSerializer(data=request.DATA)
    if serialized.is_valid():
        User.objects.create_user(
            serialized.init_data['email'],
            serialized.init_data['username'],
            serialized.init_data['password']
        )
        return Response(serialized.data, status=status.HTTP_201_CREATED)
    else:
        return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)
'''