# Generated by Django 3.0.5 on 2020-08-14 02:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0002_auto_20200814_0758'),
    ]

    operations = [
        migrations.AlterField(
            model_name='register',
            name='email',
            field=models.EmailField(max_length=100, unique=True),
        ),
    ]
