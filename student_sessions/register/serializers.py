from django.contrib.auth.models import User
from rest_framework import serializers

class registerSerializers( serializers.ModelSerializer ):

    password1 = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ['id','username','email','password','password1']
        extra_kwargs={
            'password': {'write_only':True}
        }
    def save(self):
        password = self.validated_data['password']
        password1 = self.validated_data['password1']
        if password != password1:
            raise serializers.ValidationError({"password" : "password enter is wrong"})
        account = User(
            username = self.validated_data['username'],
            email = self.validated_data['email'],
            password = self.validated_data['password'],
            )
        account.save()
        return account
