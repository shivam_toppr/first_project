from django.urls import path,include
from .views import register_view,register_detail
from . import views
urlpatterns = [
    path('register/',views.register_view, name="register"),
    path('detail/<int:pk>',views.register_detail, name="detail"),
]