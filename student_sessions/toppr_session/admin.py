from django.contrib import admin
from .models import Session, SlotRange, Slot
# Register your models here.

admin.site.register(Session)
admin.site.register(SlotRange)
admin.site.register(Slot)