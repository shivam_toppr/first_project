from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from rest_framework import viewsets, permissions
import json
import logging
from .models import Session, Slot, SlotRange
from .serializers import SessionSerializer, SlotSerializer, SlotRangeSerializer, UserSerializer
from .permissions import IsOwnerOrReadOnly, IsStaffOrTargetUser


class SessionView(viewsets.ModelViewSet):
    queryset = Session.objects.all().order_by('-subject')
    serializer_class = SessionSerializer
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        permissions.IsAdminUser
    )

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)



class SessionDetailView(viewsets.ReadOnlyModelViewSet):

    queryset = Slot.objects.all()
    serializer_class = SlotSerializer
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)
    
    def get_queryset(self):
        queryset = Slot.objects.filter(session_name__id=1).select_related('session_name__owner')
        #session_id = self.request.query_params.get('session_name')
        return queryset

    # def get_queryset(self):
    #     queryset = Slot.objects.filter(session_name__owner=self.request.user).select_related('session')
    #     #session_id = self.request.query_params.get('session_name')
    #     return queryset



class SlotView(viewsets.ModelViewSet):
    queryset = Slot.objects.all().order_by('-id')
    serializer_class = SlotSerializer
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly
    )

    def get_queryset(self):
        user = self.request.user
        queryset = self.queryset.filter(owner=user)
        return queryset

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)



    # def perform_create(self, serializer):
    #     serializer.save(owner=self.request.user)




class SlotRangeView(viewsets.ModelViewSet):
    queryset = SlotRange.objects.all()
    serializer_class = SlotRangeSerializer
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)



class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        # allow non-authenticated user to create via POST
        return (permissions.AllowAny() if self.request.method == 'POST' else IsStaffOrTargetUser()),

    def perform_create(self, serializer):
        password = make_password(self.request.data['password'])
        serializer.save(password=password)

    def perform_update(self, serializer):
        password = make_password(self.request.data['password'])
        serializer.save(password=password)


'''
class ShowSession(generics.ListAPIView):
    serializer_class = SlotSerializer
    def get_queryset(self):
        queryset = Slot.objects.all()
        student =  self.request.query_params.get('student', None)
        administrator =  self.request.query_params.get('administrator', None)
        print(student)
        print(administrator)
        if student is not None and administrator is not None:
            try:
                queryset=queryset.filter( Q(user_id=student) | Q(user_id=administrator))
            except:
                queryset=None
        return queryset
'''

'''
@api_view(['POST'])
def create_auth(request):
    serialized = UserSerializer(data=request.DATA)
    if serialized.is_valid():
        User.objects.create_user(
            serialized.init_data['email'],
            serialized.init_data['username'],
            serialized.init_data['password']
        )
        return Response(serialized.data, status=status.HTTP_201_CREATED)
    else:
        return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)
'''