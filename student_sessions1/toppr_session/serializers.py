from rest_framework import serializers
from .models import Session, Slot, SlotRange
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model


class SessionSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    class Meta:
        model = Session
        fields = '__all__'


class SlotSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    #session = SessionSerializer(read_only = True, many=True)
    class Meta:
        model = Slot
        fields = '__all__'


class SlotRangeSerializer(serializers.ModelSerializer):
    class Meta:
        model = SlotRange
        fields = '__all__'

