from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from rest_framework import viewsets, permissions, generics
import json
import logging
from .models import Session, Slot, SlotRange
from .serializers import SessionSerializer, SlotSerializer, SlotRangeSerializer
from .permissions import IsOwnerOrReadOnly, IsStaffOrTargetUser
from django.http import Http404
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response



class SessionView(viewsets.ModelViewSet):
    queryset = Session.objects.all().order_by('-subject')
    serializer_class = SessionSerializer
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        permissions.IsAdminUser
    )

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)



class SessionDetailView(APIView):
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)
    def get_object(self, pk):
        try:
            return Slot.objects.filter(session__id=pk)
        except Slot.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        slot = self.get_object(pk)
        serializer = SlotSerializer(slot, many=True)
        return Response(serializer.data)

class SlotView(viewsets.ModelViewSet):
    queryset = Slot.objects
    serializer_class = SlotSerializer

    def get_queryset(self):
        user = self.request.user
        queryset = self.queryset.filter(owner=user).select_related('session')
        
        return queryset
    def perform_create(self, serializer):
        user = self.request.user
        qs = self.queryset.session.all()#filter(owner=user).select_related('session')
        # sessions = self.queryset
        print(qs)
        sessions = Slot.objects.filter(owner=user).select_related('session__id')
        for session in sessions:
            if session.id == self.sessions.session.id:
                return Response("Already enrolled for another session in this slot", status=status.HTTP_400_BAD_REQUEST)

        serializer.save(owner=self.request.user)

# class SlotView(APIView):
#     permission_classes = (
#         permissions.IsAuthenticatedOrReadOnly,
#         IsOwnerOrReadOnly
#     )
#     def get_object(self, user):
#         try:
#             return Slot.objects.filter(owner=user)
#         except Slot.DoesNotExist:
#             raise Http404
#     def get(self, request, format=None):
#         user = self.request.user
#         slot = self.get_object(user)
#         serializer = SlotSerializer(slot, many=True)
#         return Response(serializer.data)

#     def post(self, request, format=None):
#         user = self.request.user
#         ids = Slot.objects.filter(owner=user).select_related('session__slotrange_id')
#         print(ids)
#         current_id = self.request.query_params.get('session_slotrange_id')
#         print(current_id)
#         serializer = SlotSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save(owner=self.request.user)
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SlotRangeView(viewsets.ModelViewSet):
    queryset = SlotRange.objects.all()
    serializer_class = SlotRangeSerializer
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)


    # queryset = Slot.objects.all().order_by('-id')
    # serializer_class = SlotSerializer
    


    # def get_queryset(self):
    #     user = self.request.user
    #     queryset = self.queryset.filter(owner=user)

    #     ids = Slot.objects.filter(owner=user).select_related('session__slotrange_id')
    #     current_id = self.request.query_params.get('session_slotrange_id')
    #     # startid = self.request.query_params.get('session__session_start')
    #     # queryset = self.queryset.filter(session__session_start = startid)
    #     for e in queryset:
    #         print(e)
    #     return queryset
    # def perform_create(self, serializer):
    #     serializer.save(owner=self.request.user)



'''
@api_view(['POST'])
def create_auth(request):
    serialized = UserSerializer(data=request.DATA)
    if serialized.is_valid():
        User.objects.create_user(
            serialized.init_data['email'],
            serialized.init_data['username'],
            serialized.init_data['password']
        )
        return Response(serialized.data, status=status.HTTP_201_CREATED)
    else:
        return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)
'''