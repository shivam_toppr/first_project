from django.contrib import admin
from .models import Session, Slot, SlotRange
# Register your models here.

admin.site.register(Session)
admin.site.register(Slot)
admin.site.register(SlotRange)
