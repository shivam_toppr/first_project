from django.db import models
from django.contrib.auth.models import User
import datetime
from django.utils import timezone
from django.conf import settings
from django.core.exceptions import ValidationError

def no_past(value):
    today = datetime.date.today()
    if value < today:
        raise ValidationError('Session Date cannot be in the Past.')

def no_past_time(value):
    current = timezone.now()
    if value <= current:
        raise ValidationError('Session Time cannot be in the Past.')
# Create your models here.

class SlotRange(models.Model):
    session_start = models.DateTimeField(help_text="Enter the start date-time of session")
    session_end = models.DateTimeField(help_text="Enter the end date-time of session")
    def clean(self):
        print(timezone.now(), self.session_start, self.session_end)
        if not (timezone.now() <= self.session_start <= self.session_end):
            raise ValidationError('Invalid start and end datetime')
        super(SlotRange, self).clean()

    def save(self, *args, **kwargs):
        self.full_clean()
        super(SlotRange, self).save(*args, **kwargs)

class Session(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='sessioncreator', on_delete=models.CASCADE)
    subject = models.CharField(max_length=50)
    description = models.CharField(max_length=500)
    #session_date = models.DateField(help_text="Enter the date of session", validators=[no_past])
    slotrangeid = models.ForeignKey(SlotRange, on_delete=models.CASCADE)
    

    def __str__(self):
        return self.subject + ":" + self.description


class Slot(models.Model):
    owner = models.CharField(max_length=50)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)

    # def __str__(self):
    #     return str(self.session.objects.all())


# class SessionDetails(models.Model):
#     sessionid = models.ForeignKey(Session, on_delete=models.CASCADE)
#     slotid = models.ForeignKey(Slot, on_delete=models.CASCADE)

#     def __str__(self):
#         return self.session_id.subject + ":" + self.session_id.description + " " + self.slot_id.owner.username