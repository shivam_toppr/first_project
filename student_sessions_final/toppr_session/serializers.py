from rest_framework import serializers
from .models import Session, Slot, SlotRange
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from rest_framework.authtoken.models import Token #undo

class UserSerializer(serializers.ModelSerializer):

    #password1 = serializers.CharField(write_only=True)

    # class Meta:
    #     model = User
    #     fields = ['id','username','email','password','password1', 'is_staff']
    #     extra_kwargs={
    #         'password': {'write_only':True}
    #     }
    # def save(self):
    #     password = self.validated_data['password']
    #     password1 = self.validated_data['password1']
    #     if password != password1:
    #         raise serializers.ValidationError({"password" : "Passwords doesn't match"})
    #     account = User(
    #         username = self.validated_data['username'],
    #         email = self.validated_data['email'],
    #         password = self.validated_data['password'],
    #         )
    #     account.save()
    #     return account
    class Meta:
        model = User
        fields = ['id', 'username', 'password','email','is_staff']
        extra_kwargs = {'password': {'write_only': True, 'required': True}}

    def create(self, validated_data):                     #undo the function
        user = User.objects.create_user(**validated_data)
        Token.objects.create(user=user)
        return user



class SessionSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    slotstart = serializers.ReadOnlyField(source='slotrangeid.session_start')
    slotend = serializers.ReadOnlyField(source='slotrangeid.session_end')
    class Meta:
        model = Session
        fields = ('id', 'owner', 'subject', 'description', 'slotrangeid', 'slotstart', 'slotend')
    # def create(self, validated_data):
    #     return SlotRange.objects.create(**validated_data)

class SlotSerializer(serializers.ModelSerializer):
    #owner = serializers.ReadOnlyField(source='owner.username')
    subject = serializers.ReadOnlyField(source='session.subject')
    description = serializers.ReadOnlyField(source='session.description')
    slotrangeid = serializers.ReadOnlyField(source='session.slotrangeid.id')
    slotstart = serializers.ReadOnlyField(source='session.slotrangeid.session_start')
    slotend = serializers.ReadOnlyField(source='session.slotrangeid.session_end')
    #session = SessionSerializer(read_only = True, many=True)
    class Meta:
        model = Slot
        fields = ('id', 'owner', 'session', 'subject', 'description', 'slotrangeid', 'slotstart', 'slotend')


class SlotRangeSerializer(serializers.ModelSerializer):
    class Meta:
        model = SlotRange
        fields = '__all__'

