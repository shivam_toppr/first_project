from django.contrib import admin
from django.urls import path, include
from .views import SessionView, SessionDetailView, SlotView, SlotRangeView, UserView
from django.views.decorators.csrf import csrf_exempt
from rest_framework import routers

router = routers.DefaultRouter()
router.register("sessions", SessionView)
router.register("addSlots", SlotRangeView)
router.register("students_slots", SlotView)
router.register("user", UserView)

urlpatterns = [
    path("", include(router.urls)),
    #path('subscribe_session/', csrf_exempt(SlotView.as_view())),
    path('sessionDetails/<int:pk>', SessionDetailView.as_view()),
    #path('slotSelect/', SlotView.as_view()),
    #path('register/', create_auth),
    #path('showsession/', ShowSession.as_view()),
]