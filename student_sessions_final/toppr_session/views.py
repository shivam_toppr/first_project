from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from rest_framework import viewsets, permissions, generics
import json
import logging
from .models import Session, Slot, SlotRange
from django.contrib.auth.models import User
from .serializers import SessionSerializer, SlotSerializer, SlotRangeSerializer, UserSerializer
#from .permissions import IsOwnerOrReadOnly, IsStaffOrTargetUser
from django.http import Http404
from rest_framework.exceptions import PermissionDenied
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication  #added by priyanshu
from rest_framework.permissions import IsAuthenticated    #added by priyanshu


class UserView(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    #permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly)






class SessionView(viewsets.ModelViewSet):
    queryset = Session.objects.all().order_by('-subject')
    serializer_class = SessionSerializer
    # permission_classes = (
    #     # permissions.IsAuthenticatedOrReadOnly,
    #     permissions.IsAdminUser
    # )
    # authentication_classes = [TokenAuthentication, ]
    # permission_classes = [IsAuthenticated, ]



    def perform_create(self, serializer):
        if self.request.user is None:
            raise PermissionDenied('User not provided')
        else:
            # details = self.request.POST
            # print(details)
            user = User.objects.filter(id = 1)
            serializer.save(owner=user[0])
        # serializer.save(owner = self.request.user)



class SessionDetailView(APIView):
    # permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)
    def get_object(self, pk):
        try:
            return Slot.objects.filter(session__id=pk)
        except Slot.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        slot = self.get_object(pk)
        serializer = SlotSerializer(slot, many=True)
        return Response(serializer.data)



class SlotView(viewsets.ModelViewSet):
    queryset = Slot.objects.all().order_by('-id')
    serializer_class = SlotSerializer

    # def get_queryset(self):
    #     #user = self.request.user
    #     print(self.request.POST)
    #     user = User.objects.filter(id = 2)
    #     print(user)
    #     queryset = self.queryset.filter(owner=user[0])
    #     return queryset
    #
    #
    #
    #def perform_create(self, serializer):
    #     #user = self.request.user
    #     user = User.objects.filter(id=2)
    #     # print(user)
    #     ids = Slot.objects.filter(owner=user[0]).prefetch_related('session__slotrangeid').values_list('session__slotrangeid', flat=True)
    #     print(ids)
    #     #current_id = self.request.POST.get('session_slotrangeid')
    #       current_id = self.request.POST
    #       print(current_id['session'])
    #       qs = Session.objects.filter(id = current_id['session']).prefetch_related('slotrangeid').values_list('slotrangeid', flat=True)
    #       print(qs[0])
    #       flag = 0
    #       for i in ids:
    #         print(i)
    #         if i==qs[0]:
    #             flag = 1
    #             break
    #         if flag:
    #             raise PermissionDenied('Slot already occupied')
    #         else:
    # #
    # #         serializer.save(owner=user[0])
    #             serializer.save()




# class SlotView(APIView):
#     permission_classes = (
#         permissions.IsAuthenticatedOrReadOnly,
#         IsOwnerOrReadOnly
#     )
#     def get_object(self, user):
#         try:
#             return Slot.objects.filter(owner=user)
#         except Slot.DoesNotExist:
#             raise Http404
#     def get(self, request, format=None):
#         user = self.request.user
#         slot = self.get_object(user)
#         serializer = SlotSerializer(slot, many=True)
#         return Response(serializer.data)

#     def post(self, request, format=None):
#         user = self.request.user
#         ids = Slot.objects.filter(owner=user).select_related('session__slotrange_id')
#         print(ids)
#         current_id = self.request.query_params.get('session_slotrange_id')
#         print(current_id)
#         serializer = SlotSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save(owner=self.request.user)
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SlotRangeView(viewsets.ModelViewSet):
    queryset = SlotRange.objects.all()
    serializer_class = SlotRangeSerializer
    # permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)


    # queryset = Slot.objects.all().order_by('-id')
    # serializer_class = SlotSerializer
    


    # def get_queryset(self):
    #     user = self.request.user
    #     queryset = self.queryset.filter(owner=user)

    #     ids = Slot.objects.filter(owner=user).select_related('session__slotrange_id')
    #     current_id = self.request.query_params.get('session_slotrange_id')
    #     # startid = self.request.query_params.get('session__session_start')
    #     # queryset = self.queryset.filter(session__session_start = startid)
    #     for e in queryset:
    #         print(e)
    #     return queryset
    # def perform_create(self, serializer):
    #     serializer.save(owner=self.request.user)



'''
@api_view(['POST'])
def create_auth(request):
    serialized = UserSerializer(data=request.DATA)
    if serialized.is_valid():
        User.objects.create_user(
            serialized.init_data['email'],
            serialized.init_data['username'],
            serialized.init_data['password']
        )
        return Response(serialized.data, status=status.HTTP_201_CREATED)
    else:
        return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)
'''