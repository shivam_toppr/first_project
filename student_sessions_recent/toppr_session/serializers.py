from rest_framework import serializers
from .models import Session, Slot, SlotRange
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model

class UserSerializer(serializers.ModelSerializer):

    password1 = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ['id','username','email','password','password1']
        extra_kwargs={
            'password': {'write_only':True}
        }
    def save(self):
        password = self.validated_data['password']
        password1 = self.validated_data['password1']
        if password != password1:
            raise serializers.ValidationError({"password" : "Passwords doesn't match"})
        account = User(
            username = self.validated_data['username'],
            email = self.validated_data['email'],
            password = self.validated_data['password'],
            )
        account.save()
        return account



class SessionSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    class Meta:
        model = Session
        fields = '__all__'


class SlotSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    #session = SessionSerializer(read_only = True, many=True)
    class Meta:
        model = Slot
        fields = '__all__'


class SlotRangeSerializer(serializers.ModelSerializer):
    class Meta:
        model = SlotRange
        fields = '__all__'

